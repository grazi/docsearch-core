package codes.probst.docsearch.packages.url;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.url.NoopUrlCleaner;
import codes.probst.framework.url.UrlResolver;

public class PackageUrlResolverTest {
	private UrlResolver<PackageModel> urlResolver;
	
	@Before
	public void setup() {
		DefaultPackageUrlResolver urlResolver = new DefaultPackageUrlResolver();
		urlResolver.setUrlCleaner(new NoopUrlCleaner());
		
		this.urlResolver = urlResolver;
	}
	
	@Test
	public void testVersionWithVersionAndProject() {
		PackageModel packagez = new PackageModel();
		packagez.setId(Long.valueOf(10l));
		packagez.setName("com.example");
		
		VersionModel version = new VersionModel();
		version.setCode("1.1");
		version.setId(Long.valueOf(0l));
		
		ProjectModel project = new ProjectModel();
		project.setId(Long.valueOf(5l));
		project.setArtifactId("junit");
		project.setGroupId("junit-group");
		
		version.setProject(project);
		packagez.setVersion(version);
		
		String url = urlResolver.resolve(packagez);
		Assert.assertEquals("/projects/junit/5/1.1/0/com.example/10", url);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testVersionWithVersionAndWithoutProject() {
		PackageModel packagez = new PackageModel();
		packagez.setId(Long.valueOf(10l));
		packagez.setName("com.example");
		
		VersionModel version = new VersionModel();
		version.setCode("1.1");
		version.setId(Long.valueOf(0l));
		
		packagez.setVersion(version);
		
		urlResolver.resolve(packagez);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testVersionWithoutVersionAndProject() {
		PackageModel packagez = new PackageModel();
		packagez.setId(Long.valueOf(10l));
		packagez.setName("com.example");
		
		urlResolver.resolve(packagez);
	}
}

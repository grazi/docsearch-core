package codes.probst.docsearch.url;

import codes.probst.framework.url.UrlResolver;

public class NoopUrlResolver<T> implements UrlResolver<T> {

	@Override
	public String resolve(T value) {
		return "/junit/url";
	}
	
}

package codes.probst.docsearch.project.url;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.framework.url.NoopUrlCleaner;
import codes.probst.framework.url.UrlResolver;

public class ProjectUrlResolverTest {
	private UrlResolver<ProjectModel> urlResolver;
	
	@Before
	public void setup() {
		DefaultProjectUrlResolver urlResolver = new DefaultProjectUrlResolver();
		urlResolver.setUrlCleaner(new NoopUrlCleaner());
		
		this.urlResolver = urlResolver;
	}
	
	@Test
	public void testProject() {
		ProjectModel project = new ProjectModel();
		project.setId(Long.valueOf(5l));
		project.setArtifactId("junit");
		project.setGroupId("junit-group");
		
		String url = urlResolver.resolve(project);
		Assert.assertEquals("/projects/junit/5", url);
	}
}

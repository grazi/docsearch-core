package codes.probst.docsearch.version.url;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.url.NoopUrlCleaner;
import codes.probst.framework.url.UrlResolver;

public class VersionUrlResolverTest {
	private UrlResolver<VersionModel> urlResolver;
	
	@Before
	public void setup() {
		DefaultVersionUrlResolver urlResolver = new DefaultVersionUrlResolver();
		urlResolver.setUrlCleaner(new NoopUrlCleaner());
		
		this.urlResolver = urlResolver;
	}
	
	@Test
	public void testVersionWithProject() {
		VersionModel version = new VersionModel();
		version.setCode("1.1");
		version.setId(Long.valueOf(0l));
		
		ProjectModel project = new ProjectModel();
		project.setId(Long.valueOf(5l));
		project.setArtifactId("junit");
		project.setGroupId("junit-group");
		
		version.setProject(project);
		
		String url = urlResolver.resolve(version);
		Assert.assertEquals("/projects/junit/5/1.1/0", url);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testVersionWithoutProject() {
		VersionModel version = new VersionModel();
		version.setCode("1.1");
		version.setId(Long.valueOf(0l));
		
		urlResolver.resolve(version);
	}
}

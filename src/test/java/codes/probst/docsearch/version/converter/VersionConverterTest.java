package codes.probst.docsearch.version.converter;

import java.time.Instant;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.facade.converter.DefaultVersionConverter;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.converter.Converter;

public class VersionConverterTest {
	private Converter<VersionModel, VersionData> converter;
	
	@Before
	public void setup() {
		converter = new DefaultVersionConverter();
	}
	
	@Test
	public void testConversion() {
		VersionModel model = new VersionModel();
		model.setCode("1.1");
		model.setId(Long.valueOf(0l));
		model.setImportedAt(Instant.now());
		model.setPackages(Arrays.asList(new PackageModel()));
		model.setProject(new ProjectModel());
		model.setRequires(Arrays.asList(new VersionModel()));
		
		VersionData data = converter.convert(model);
		
		Assert.assertNotNull(data);
		Assert.assertEquals("1.1", data.getCode());
		Assert.assertEquals(Long.valueOf(0l), data.getId());
		
		Assert.assertNull(data.getUrl());
		Assert.assertNull(data.getPackages());
		Assert.assertNull(data.getProject());
	}
}

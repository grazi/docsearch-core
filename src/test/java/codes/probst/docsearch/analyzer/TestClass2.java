package codes.probst.docsearch.analyzer;

public class TestClass2 {
	private TestClass2_2 clazz;
	
	
	public TestClass2_2 getClazz() {
		return clazz;
	}
	
	public void setClazz(TestClass2_2 clazz) {
		this.clazz = clazz;
	}
	
	public static class TestClass2_2 {
		private String string;
		
		public String getString() {
			return string;
		}
		
		public void setString(String string) {
			this.string = string;
		}
	}
}

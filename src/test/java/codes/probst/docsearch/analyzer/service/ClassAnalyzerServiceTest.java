package codes.probst.docsearch.analyzer.service;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.analyzer.TestClass1;
import codes.probst.docsearch.analyzer.TestClass2;
import codes.probst.docsearch.analyzer.TestClass3;
import codes.probst.docsearch.analyzer.TestClass2.TestClass2_2;
import codes.probst.docsearch.test.util.IOUtil;

public class ClassAnalyzerServiceTest {
	private ClassAnalyzerService analyzer;
	
	@Before
	public void setup() {
		analyzer = new DefaultClassAnalyzerService();
	}
	
	@Test
	public void testSimpleClass() throws IOException {
		byte[] data =  IOUtil.readAll(getClass().getResourceAsStream("/codes/probst/docsearch/analyzer/TestClass1.class"), 1024);
		ClassAnalysis classAnalysis = analyzer.analyzeClass(data);
		
		Assert.assertNotNull(classAnalysis);
		Assert.assertEquals(TestClass1.class.getName(), classAnalysis.getClassName());
		
		Collection<MethodAnalysis> methodAnalysises = classAnalysis.getMethodAnalysis();
		
		Assert.assertNotNull(methodAnalysises);
		Assert.assertEquals(2, methodAnalysises.size());
		
		Iterator<MethodAnalysis> it = methodAnalysises.iterator();
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(Modifier.PUBLIC, methodAnalysis.getAccess());
			Assert.assertEquals("getString", methodAnalysis.getName());
			Assert.assertEquals(String.class.getName(), methodAnalysis.getReturnType());
			Assert.assertTrue(methodAnalysis.getParameters().isEmpty());
		}
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(Modifier.PUBLIC, methodAnalysis.getAccess());
			Assert.assertEquals("setString", methodAnalysis.getName());
			Assert.assertEquals(Void.TYPE.getName(), methodAnalysis.getReturnType());
			Assert.assertEquals(1, methodAnalysis.getParameters().size());
			Assert.assertEquals(String.class.getName(), methodAnalysis.getParameters().get(0));
		}
		
		Collection<ConstructorAnalysis> constructorAnalysises = classAnalysis.getConstructorAnalysis();
		
		Assert.assertNotNull(constructorAnalysises);
		Assert.assertEquals(1, constructorAnalysises.size());
		
		Iterator<ConstructorAnalysis> cIt = constructorAnalysises.iterator();
		
		{
			ConstructorAnalysis constructorAnalysis = cIt.next();
			Assert.assertEquals(Modifier.PUBLIC, constructorAnalysis.getAccess());
			Assert.assertTrue(constructorAnalysis.getParameters().isEmpty());
		}
		
	}
	
	@Test
	public void testInnerClassHost() throws IOException {
		byte[] data =  IOUtil.readAll(getClass().getResourceAsStream("/codes/probst/docsearch/analyzer/TestClass2.class"), 1024);
		ClassAnalysis classAnalysis = analyzer.analyzeClass(data);
		
		Assert.assertNotNull(classAnalysis);
		Assert.assertEquals(TestClass2.class.getName(), classAnalysis.getClassName());
		
		Collection<MethodAnalysis> methodAnalysises = classAnalysis.getMethodAnalysis();
		
		Assert.assertNotNull(methodAnalysises);
		Assert.assertEquals(2, methodAnalysises.size());
		
		Iterator<MethodAnalysis> it = methodAnalysises.iterator();
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(Modifier.PUBLIC, methodAnalysis.getAccess());
			Assert.assertEquals("getClazz", methodAnalysis.getName());
			Assert.assertEquals(TestClass2_2.class.getName(), methodAnalysis.getReturnType());
			Assert.assertTrue(methodAnalysis.getParameters().isEmpty());
		}
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(Modifier.PUBLIC, methodAnalysis.getAccess());
			Assert.assertEquals("setClazz", methodAnalysis.getName());
			Assert.assertEquals(Void.TYPE.getName(), methodAnalysis.getReturnType());
			Assert.assertEquals(1, methodAnalysis.getParameters().size());
			Assert.assertEquals(TestClass2_2.class.getName(), methodAnalysis.getParameters().get(0));
		}
		
		Collection<ConstructorAnalysis> constructorAnalysises = classAnalysis.getConstructorAnalysis();
		
		Assert.assertNotNull(constructorAnalysises);
		Assert.assertEquals(1, constructorAnalysises.size());
		
		Iterator<ConstructorAnalysis> cIt = constructorAnalysises.iterator();
		
		{
			ConstructorAnalysis constructorAnalysis = cIt.next();
			Assert.assertEquals(Modifier.PUBLIC, constructorAnalysis.getAccess());
			Assert.assertTrue(constructorAnalysis.getParameters().isEmpty());
		}
	}
	
	@Test
	public void testInnerClass() throws IOException {
		byte[] data =  IOUtil.readAll(getClass().getResourceAsStream("/codes/probst/docsearch/analyzer/TestClass2$TestClass2_2.class"), 1024);
		ClassAnalysis classAnalysis = analyzer.analyzeClass(data);
		
		Assert.assertNotNull(classAnalysis);
		Assert.assertEquals(TestClass2_2.class.getName(), classAnalysis.getClassName());
		
		Collection<MethodAnalysis> methodAnalysises = classAnalysis.getMethodAnalysis();
		
		Assert.assertNotNull(methodAnalysises);
		Assert.assertEquals(2, methodAnalysises.size());
		
		Iterator<MethodAnalysis> it = methodAnalysises.iterator();
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(Modifier.PUBLIC, methodAnalysis.getAccess());
			Assert.assertEquals("getString", methodAnalysis.getName());
			Assert.assertEquals(String.class.getName(), methodAnalysis.getReturnType());
			Assert.assertTrue(methodAnalysis.getParameters().isEmpty());
		}
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(Modifier.PUBLIC, methodAnalysis.getAccess());
			Assert.assertEquals("setString", methodAnalysis.getName());
			Assert.assertEquals(Void.TYPE.getName(), methodAnalysis.getReturnType());
			Assert.assertEquals(1, methodAnalysis.getParameters().size());
			Assert.assertEquals(String.class.getName(), methodAnalysis.getParameters().get(0));
		}
		
		Collection<ConstructorAnalysis> constructorAnalysises = classAnalysis.getConstructorAnalysis();
		
		Assert.assertNotNull(constructorAnalysises);
		Assert.assertEquals(1, constructorAnalysises.size());
		
		Iterator<ConstructorAnalysis> cIt = constructorAnalysises.iterator();
		
		{
			ConstructorAnalysis constructorAnalysis = cIt.next();
			Assert.assertEquals(Modifier.PUBLIC, constructorAnalysis.getAccess());
			Assert.assertTrue(constructorAnalysis.getParameters().isEmpty());
		}
	}
	
	@Test
	public void testMethodVisibility() throws IOException {
		byte[] data =  IOUtil.readAll(getClass().getResourceAsStream("/codes/probst/docsearch/analyzer/TestClass3.class"), 1024);
		ClassAnalysis classAnalysis = analyzer.analyzeClass(data);
		
		Assert.assertNotNull(classAnalysis);
		Assert.assertEquals(TestClass3.class.getName(), classAnalysis.getClassName());
		
		Collection<MethodAnalysis> methodAnalysises = classAnalysis.getMethodAnalysis();
		
		Assert.assertNotNull(methodAnalysises);
		Assert.assertEquals(4, methodAnalysises.size());
		
		Iterator<MethodAnalysis> it = methodAnalysises.iterator();
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(Modifier.PRIVATE, methodAnalysis.getAccess());
			Assert.assertEquals("privateMethod", methodAnalysis.getName());
			Assert.assertEquals(Void.TYPE.getName(), methodAnalysis.getReturnType());
			Assert.assertTrue(methodAnalysis.getParameters().isEmpty());
		}
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(0, methodAnalysis.getAccess());
			Assert.assertEquals("packagePrivateMethod", methodAnalysis.getName());
			Assert.assertEquals(Void.TYPE.getName(), methodAnalysis.getReturnType());
			Assert.assertTrue(methodAnalysis.getParameters().isEmpty());
		}
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(Modifier.PROTECTED, methodAnalysis.getAccess());
			Assert.assertEquals("protectedMethod", methodAnalysis.getName());
			Assert.assertEquals(Void.TYPE.getName(), methodAnalysis.getReturnType());
			Assert.assertTrue(methodAnalysis.getParameters().isEmpty());
		}
		
		{
			MethodAnalysis methodAnalysis = it.next();
			Assert.assertEquals(Modifier.PUBLIC, methodAnalysis.getAccess());
			Assert.assertEquals("publicMethod", methodAnalysis.getName());
			Assert.assertEquals(Void.TYPE.getName(), methodAnalysis.getReturnType());
			Assert.assertTrue(methodAnalysis.getParameters().isEmpty());
		}

		
		Collection<ConstructorAnalysis> constructorAnalysises = classAnalysis.getConstructorAnalysis();
		
		Assert.assertNotNull(constructorAnalysises);
		Assert.assertEquals(1, constructorAnalysises.size());
		
		Iterator<ConstructorAnalysis> cIt = constructorAnalysises.iterator();
		
		{
			ConstructorAnalysis constructorAnalysis = cIt.next();
			Assert.assertEquals(Modifier.PUBLIC, constructorAnalysis.getAccess());
			Assert.assertTrue(constructorAnalysis.getParameters().isEmpty());
		}
	}
	
}

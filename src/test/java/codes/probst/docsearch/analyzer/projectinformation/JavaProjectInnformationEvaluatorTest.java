package codes.probst.docsearch.analyzer.projectinformation;

import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.test.util.IOUtil;


public class JavaProjectInnformationEvaluatorTest {
	private ProjectInformationEvaluator evaluator;
	
	@Before
	public void setup() {
		evaluator = new JavaProjectInformationEvaluator();
	}
	
	@Test
	public void testDummyJavaZip() throws IOException {
		try(ZipInputStream is = new ZipInputStream(getClass().getResourceAsStream("/codes/probst/docsearch/analyzer/projectinformation/java.zip"))) {
			//META-INF folder
			is.getNextEntry();
			
			//MANIFEST.MF
			ZipEntry entry = is.getNextEntry();
			Assert.assertTrue(evaluator.canHandleEntry(entry));
			
			ProjectInformation information = evaluator.evaluate(IOUtil.readAll(is, 1024));
			
			Assert.assertNotNull(information);
			Assert.assertEquals("java", information.getGroupId());
			Assert.assertEquals("jdk", information.getArtifactId());
			Assert.assertEquals("1.8", information.getVersion());
			Assert.assertEquals(ProjectType.JAVA, information.getType());
		}
	}
	
}

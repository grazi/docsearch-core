package codes.probst.docsearch.clazz.dao;

import java.util.Collection;
import java.util.Iterator;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;

public class ClassDaoTest {
	private ClassDao classDao;
	private EntityManager entityManager;
	
	@Before
	public void setup() {
		entityManager = Persistence.createEntityManagerFactory("junit").createEntityManager();
		JpaClassDao dao = new JpaClassDao();
		dao.setEntityManager(entityManager);
		classDao = dao;
	}
	
	@Test
	public void testFindByPackageId() {
		entityManager.getTransaction().begin();
		ProjectModel project = new ProjectModel();
		project.setArtifactId("junit");
		project.setGroupId("com.example");
		entityManager.persist(project);
		
		VersionModel version = new VersionModel();
		version.setCode("junit");
		version.setProject(project);
		entityManager.persist(version);
		
		
		PackageModel p = new PackageModel();
		p.setName("codes.probst");
		p.setVersion(version);
		
		entityManager.persist(p);
		
		ClassModel c1 = new ClassModel();
		c1.setName("UnitTest1");
		c1.setPackageModel(p);
		
		ClassModel c2 = new ClassModel();
		c2.setName("UnitTest2");
		c2.setPackageModel(p);
		
		entityManager.persist(c1);
		entityManager.persist(c2);
		
		Pageable pageable = new Pageable();
		Sort sort = new Sort();
		sort.setDirection(SortDirection.ASCENDING);
		sort.setName("name");
		pageable.setSort(new Sort[] { sort });
		pageable.setCurrentPage(0);
		pageable.setPageSize(10);
		Collection<ClassModel> classes = classDao.findByPackageId(p.getId(), pageable);
		
		Assert.assertNotNull(classes);
		Assert.assertEquals(2, classes.size());
		
		Iterator<ClassModel> it = classes.iterator();
		{
			ClassModel clazz = it.next();
			
			Assert.assertEquals("UnitTest1", clazz.getName());
		}
		
		{
			ClassModel clazz = it.next();
			
			Assert.assertEquals("UnitTest2", clazz.getName());
		}

		entityManager.getTransaction().rollback();
	}
}

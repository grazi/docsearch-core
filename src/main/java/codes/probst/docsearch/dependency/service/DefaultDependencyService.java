package codes.probst.docsearch.dependency.service;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.dependency.Dependency;
import codes.probst.docsearch.dependency.DependencyResolver;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.docsearch.version.service.VersionService;

public class DefaultDependencyService implements DependencyService {
	private Map<ProjectType, DependencyResolver> resolvers;
	private VersionService versionService;
	
	@Override
	public Collection<Dependency> getDependencies(ProjectType type, byte[] bytes) {
		DependencyResolver resolver = resolvers.get(type);
		
		Collection<Dependency> dependencies = resolver == null ? null : resolver.resolve(bytes);
		
		return dependencies == null ? Collections.emptyList() : dependencies;
	}
	
	@Override
	public Optional<VersionModel> getJavaVersion(byte[] bytes) {
		try(ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
			ZipInputStream is = new ZipInputStream(bais)) {
			
			for(ZipEntry entry = null; (entry = is.getNextEntry()) != null;) {
				if(entry.getName().endsWith(".class")) {
					DataInputStream dis = new DataInputStream(is);
					int magicNumber = dis.readInt();
					if(magicNumber != 0xcafebabe) {
						continue;
					}
					int minor = dis.readUnsignedShort();
					int major = dis.readUnsignedShort();
					
					int javaVersion = major - 44;
					
					for(int i = javaVersion; i <= 9; i++) {
						Optional<VersionModel> optionalVersion = versionService.getByFullQualifier("java", "jdk", "1." + i);
						if(optionalVersion.isPresent()) {
							return optionalVersion;
						}
					}
					
					break;
				}
			}
		} catch (IOException e) {
			throw new IllegalArgumentException("could nor read zip", e);
		}
		
		return Optional.empty();
	}
	
	public void setResolvers(Map<ProjectType, DependencyResolver> resolvers) {
		this.resolvers = resolvers;
	}
	
	public void setVersionService(VersionService versionService) {
		this.versionService = versionService;
	}
}

package codes.probst.docsearch.dependency;

import java.util.Collection;
import java.util.Collections;

public class NoopDependencyResolver implements DependencyResolver {

	@Override
	public Collection<Dependency> resolve(byte[] bytes) {
		return Collections.emptyList();
	}
}

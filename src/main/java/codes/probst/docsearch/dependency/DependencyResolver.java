package codes.probst.docsearch.dependency;

import java.util.Collection;

public interface DependencyResolver {

	public abstract Collection<Dependency> resolve(byte[] bytes);

}
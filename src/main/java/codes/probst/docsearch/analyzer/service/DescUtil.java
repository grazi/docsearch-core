package codes.probst.docsearch.analyzer.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility to handle the descriptor.
 * @author Benjamin Probst
 *
 */
public class DescUtil {
	private static final Map<String, String> SIGNATURE_TO_TYPE = new HashMap<>();
	private static final Map<String, String> TYPE_TO_SIGNATURE = new HashMap<>();
	private static final Map<String, Class<?>> TYPE_TO_CLASS = new HashMap<>();
	
	static {
		SIGNATURE_TO_TYPE.put("Z", Boolean.TYPE.getName());
		SIGNATURE_TO_TYPE.put("B", Byte.TYPE.getName());
		SIGNATURE_TO_TYPE.put("C", Character.TYPE.getName());
		SIGNATURE_TO_TYPE.put("S", Short.TYPE.getName());
		SIGNATURE_TO_TYPE.put("I", Integer.TYPE.getName());
		SIGNATURE_TO_TYPE.put("J", Long.TYPE.getName());
		SIGNATURE_TO_TYPE.put("F", Float.TYPE.getName());
		SIGNATURE_TO_TYPE.put("D", Double.TYPE.getName());
		SIGNATURE_TO_TYPE.put("V", Void.TYPE.getName());
		
		SIGNATURE_TO_TYPE.forEach((key, value) -> TYPE_TO_SIGNATURE.put(value, key));
		
		TYPE_TO_CLASS.put(Boolean.TYPE.getName(), Boolean.TYPE);
		TYPE_TO_CLASS.put(Byte.TYPE.getName(), Byte.TYPE);
		TYPE_TO_CLASS.put(Character.TYPE.getName(), Character.TYPE);
		TYPE_TO_CLASS.put(Short.TYPE.getName(), Short.TYPE);
		TYPE_TO_CLASS.put(Integer.TYPE.getName(), Integer.TYPE);
		TYPE_TO_CLASS.put(Long.TYPE.getName(), Long.TYPE);
		TYPE_TO_CLASS.put(Float.TYPE.getName(), Float.TYPE);
		TYPE_TO_CLASS.put(Double.TYPE.getName(), Double.TYPE);
		TYPE_TO_CLASS.put(Void.TYPE.getName(), Void.TYPE);
	}
	
	/**
	 * Parses a descriptor and retrieves all contained classes.
	 * @param desc the descriptor
	 * @return List of all classes
	 */
	public static List<String> getClassesFromDescription(String desc) {
		List<String> parameters = new ArrayList<>();
		
		char[] characters = desc.toCharArray();
		boolean array = false;
		for(int i = 0; i < characters.length; i++) {
			switch(characters[i]) {
				case 'L':	int start = i + 1;
							do {
							} while(characters[++i] != ';');
							char[] clazzCharacters = Arrays.copyOfRange(characters, start, i);
							
							String clazzName = new String(clazzCharacters).replaceAll("/", ".");
							if(array) {
								clazzName+= "[]";
								array = false;
							}
							parameters.add(clazzName);
							
							break;
				case '[':	array = true;
							break;
				default:	parameters.add(SIGNATURE_TO_TYPE.get(String.valueOf(characters[i])) + (array ? "[]" : ""));
							array = false;
							break;
			}
		}
		
		return parameters;
	}
}

package codes.probst.docsearch.analyzer.service;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codes.probst.docsearch.method.model.MethodUsageModel;

public class DefaultCodeSnippedService implements CodeSnippedService {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultCodeSnippedService.class);
	
	private static final Pattern CLASS_PATTERN = Pattern.compile("(?m)^[ \t]*([\\$_\\w\\<\\>\\w\\s\\[\\]]+)\\s*class\\s+([\\$_\\w\\<\\>\\w\\s\\[\\]]+)");
	private static final Pattern METHOD_PATTERN = Pattern.compile("(?m)^[ \t]*(public|private|protected|static|final|native|synchronized|abstract|transient)?\\s+[\\$_\\w\\<\\>\\w\\s\\[\\]]*\\s+[\\$_\\w]+\\([^\\)]*\\)?\\s*\\{");
	private static final Pattern ATTRIBUTE_PATTERN = Pattern.compile("(?m)^[ \t]*(public|private|protected|static|final|native|synchronized|abstract|transient)?\\s+[\\$_\\w\\<\\>\\w\\s\\[\\]]*\\s+[\\$_\\w]+\\s*=");
	
	private int maximumNumberOfLines;
	
	@Override
	public String getCodeSnipped(String sourceCode, MethodUsageModel methodUsage) {
		if(methodUsage.getLineNumber() < 1) {
			return null;
		}
		
		try {
			String[] lines =  sourceCode.replaceAll("\r", "").split("[\n]");
			
			if(lines.length <= methodUsage.getLineNumber()) {
				return null;
			}
			
			int end = expandLineNumber(lines, Math.min(methodUsage.getLineNumber() - 1, lines.length), methodUsage.getMethod().getName());
			if(end == -1) {
				return null;
			}
			String snipped = toStringOfRange(lines, Math.max(0, end - maximumNumberOfLines), end);
			snipped = limitToContainingClass(snipped);
			snipped = limitByType(snipped);
			snipped = trimEmptyStartAndEnd(snipped);
				
			return snipped;
		} catch(Exception e) {
			LOG.warn("could not get code snipped", e);
		}
		return null;
	}
	
	private int expandLineNumber(String[] lines, int lineNumber, String methodName) {
		int index = lines[lineNumber].indexOf(methodName);
		while(index == -1 && lineNumber > 0) {
			index = lines[--lineNumber].indexOf(methodName);
		}
		if(index == -1) {
			return -1;
		}
		String splitAfterMethodCall = lines[lineNumber].substring(index);
		int openBrackets = StringUtils.countMatches(splitAfterMethodCall, '(');
		if(openBrackets == 0) {
			while(openBrackets == 0) {
				openBrackets = StringUtils.countMatches(lines[++lineNumber], '(');
			}
			openBrackets -= StringUtils.countMatches(lines[lineNumber], ')');
		} else {
			openBrackets -= StringUtils.countMatches(splitAfterMethodCall, ')');
		}
		if(openBrackets > 0) {
			lineNumber++;
			for(; lineNumber < lines.length && openBrackets > 0; lineNumber++) {
				openBrackets += StringUtils.countMatches(lines[lineNumber], '(');
				openBrackets -= StringUtils.countMatches(lines[lineNumber], ')');
			}
		} else {
			lineNumber++;
		}
		
		return lineNumber;
	}
	
	private String limitToContainingClass(String source) {
		Matcher classMatcher = CLASS_PATTERN.matcher(source);
		int start = 0;
		while (classMatcher.find()) {
			start = classMatcher.start();
		}
		
		return start == 0 ? source : source.substring(start);
	}
	
	private String limitByType(String source) {
		//limit to method
		int start = 0;
		Matcher methodMatcher = METHOD_PATTERN.matcher(source);
		while (methodMatcher.find()) {
			start = methodMatcher.start();
		}
		if (start == 0) {
			//limit to attribute
			Matcher attributeMatcher = ATTRIBUTE_PATTERN.matcher(source);
			while (attributeMatcher.find()) {
				start = attributeMatcher.start();
			}
		}
		
		return start == 0 ? source : source.substring(start);
	}
	
	private String trimEmptyStartAndEnd(String source) {
		String[] lines = source.split("\n");
		int start = 0;
		int end = lines.length;
		
		while (lines[start].trim().isEmpty()) {
			start++;
		}
		while (lines[end - 1].trim().isEmpty()) {
			end--;
		}
		
		return toStringOfRange(lines, start, end);
	}
	
	private String toStringOfRange(String[] array, int start, int end) {
		return Arrays.stream(Arrays.copyOfRange(array, start, end)).collect(Collectors.joining("\n"));
	}
	
	public void setMaximumNumberOfLines(int maximumNumberOfLines) {
		this.maximumNumberOfLines = maximumNumberOfLines;
	}
}

package codes.probst.docsearch.analyzer.service;

import java.util.List;

/**
 * Holds all data about a analyzed method.
 * @author Benjamin Probst
 *
 */
public class MethodAnalysis {
	private int access;
	private String name;
	private String returnType;
	private String signature;
	private List<String> parameters;
	
	public MethodAnalysis(int access, String name, String signature, String returnType, List<String> parameters) {
		this.access = access;
		this.name = name;
		this.signature = signature;
		this.returnType = returnType;
		this.parameters = parameters;
	}

	/**
	 * Returns the access level.
	 * @return
	 */
	public int getAccess() {
		return access;
	}

	/**
	 * Returns the name of the method.
	 * @return Name of the method
	 */
	public String getName() {
		return name;
	}

	public String getSignature() {
		return signature;
	}
	
	/**
	 * Returns the return type of the method.
	 * @return Return type of the method
	 */
	public String getReturnType() {
		return returnType;
	}

	/**
	 * Returns all parameters of the method.
	 * @return All parameters
	 */
	public List<String> getParameters() {
		return parameters;
	}

}

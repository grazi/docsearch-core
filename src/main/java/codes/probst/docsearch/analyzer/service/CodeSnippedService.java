package codes.probst.docsearch.analyzer.service;

import codes.probst.docsearch.method.model.MethodUsageModel;

public interface CodeSnippedService {

	String getCodeSnipped(String sourceCode, MethodUsageModel methodUsage);

}
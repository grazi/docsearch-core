package codes.probst.docsearch.analyzer.service;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;

/**
 * Implementation of the {@code ClassAnalyzerService} based on ASM.
 * @author Benjamin Probst
 *
 */
public class DefaultClassAnalyzerService implements ClassAnalyzerService {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getClassNameFromBytes(byte[] bytes) {
		ClassAnalysisAnalyzerClassVisitor visitor = new ClassAnalysisAnalyzerClassVisitor();
		handleVisitorForBytes(visitor, bytes);
		
		return visitor.getClassName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClassAnalysis analyzeClass(byte[] bytes) {
		ClassAnalysisAnalyzerClassVisitor visitor = new ClassAnalysisAnalyzerClassVisitor();
		handleVisitorForBytes(visitor, bytes);
		
		return new ClassAnalysis(visitor.getClassName(), visitor.getMethods(), visitor.getConstructors(), visitor.getMethodUsageAnalysises());
	}
	
	/**
	 * Calls the visitor for the specific class bytes.
	 * @param visitor The visitor to call
	 * @param bytes The class bytes
	 */
	protected void handleVisitorForBytes(ClassVisitor visitor, byte[] bytes){
		ClassReader reader = new ClassReader(bytes);
		reader.accept(visitor, 0);
	}
}

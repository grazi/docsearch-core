package codes.probst.docsearch.analyzer.service;

import java.util.List;

public class MethodUsageAnalysis {
	private String callingClassName;
	private String calledClassName;
	private String calledMethodName;
	private List<String> parameters;
	private String signature;
	private String returnType;
	private int lineNumber;
	
	public MethodUsageAnalysis(String callingClassName, String calledClassName, String calledMethodName, List<String> parameters, String signature, String returnType, int lineNumber) {
		this.callingClassName = callingClassName;
		this.calledClassName = calledClassName;
		this.calledMethodName = calledMethodName;
		this.parameters = parameters;
		this.signature = signature;
		this.returnType = returnType;
		this.lineNumber = lineNumber;
	}

	public String getCallingClassName() {
		return callingClassName;
	}

	public String getCalledClassName() {
		return calledClassName;
	}

	public String getCalledMethodName() {
		return calledMethodName;
	}

	public List<String> getParameters() {
		return parameters;
	}
	
	public String getSignature() {
		return signature;
	}

	public String getReturnType() {
		return returnType;
	}

	public int getLineNumber() {
		return lineNumber;
	}

}

package codes.probst.docsearch.analyzer.projectinformation;

import java.util.Optional;

/**
 * Analyzer to retrieve {@code ProjectInformation} from the complete {@code byte}s of a project.
 * @author Benjamin Probst
 *
 */
public interface ProjectInformationAnalyzer {

	/**
	 * Analyzes the data of a project to retrieve its {@code ProjectInformation}.
	 * @param data The data of the project
	 * @return The information of a project
	 */
	public abstract Optional<ProjectInformation> analyze(byte[] data);

}
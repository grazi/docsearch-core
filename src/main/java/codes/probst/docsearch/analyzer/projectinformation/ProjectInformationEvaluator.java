package codes.probst.docsearch.analyzer.projectinformation;

import java.util.zip.ZipEntry;

/**
 * Evaluates if a {@code ZipEntry} can be handled by the {@code ProjectInformationEvaluator} and if it can be
 * the evaluate method returns the  {@code ProjectInformation} from the {@code ZipEntry}.
 * 
 * @author Benjamin Probst
 *
 */
public interface ProjectInformationEvaluator {

	/**
	 * Checks if the given {@code ZipEntry} could be handled by this  {@code ProjectInformationEvaluator}.
	 * @param entry the entry to be checked
	 * @return {@code true} if the {@code ProjectInformationEvaluator} supports this  {@code ZipEntry} and {@code false} if not
	 */
	public boolean canHandleEntry(ZipEntry entry);
	
	/**
	 * Evaluates the {@code ProjectInformation} based on the given {@code byte}s.
	 * @param bytes The bytes to evaluate
	 * @return The information of a project based on the given bytes
	 */
	public ProjectInformation evaluate(byte[] bytes);
	
}

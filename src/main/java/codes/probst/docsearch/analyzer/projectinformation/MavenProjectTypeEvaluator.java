package codes.probst.docsearch.analyzer.projectinformation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.zip.ZipEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MavenProjectTypeEvaluator implements ProjectInformationEvaluator {
	private static final Logger LOG = LoggerFactory.getLogger(MavenProjectTypeEvaluator.class);
	
	@Override
	public ProjectInformation evaluate(byte[] bytes) {
		ProjectInformation information = null;
		
		Properties properties = new Properties();
		try {
			properties.load(new ByteArrayInputStream(bytes));

			String groupId = properties.getProperty("groupId");
			String artifactId = properties.getProperty("artifactId");
			String version = properties.getProperty("version");
			
			information = new ProjectInformation(ProjectType.MAVEN, groupId, artifactId, version);
		} catch(IOException e) {
			LOG.error("Could not load Properties", e);
		}
		
		return information;
	}

	
	@Override
	public boolean canHandleEntry(ZipEntry entry) {
		return entry.getName().startsWith("META-INF/maven/") && entry.getName().endsWith("/pom.properties");
	}
	
}

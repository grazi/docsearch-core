package codes.probst.docsearch.analyzer.projectinformation;

/**
 * Represents the analyzed information about a project.
 * @author Benjamin Probst
 *
 */
public class ProjectInformation {
	private ProjectType type;
	private String groupId;
	private String artifactId;
	private String version;

	public ProjectInformation(ProjectType type, String groupId, String artifactId, String version) {
		this.type = type;
		this.groupId = groupId;
		this.artifactId = artifactId;
		this.version = version;
	}

	/**
	 * Returns the type of the project.
	 * @return The type
	 */
	public ProjectType getType() {
		return type;
	}

	/**
	 * Returns the group id of the project.
	 * @return The groud id
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * Returns the artifact id of the project.
	 * @return The artifact id
	 */
	public String getArtifactId() {
		return artifactId;
	}

	/**
	 * Returns the version of the project
	 * @return The version
	 */
	public String getVersion() {
		return version;
	}
}

package codes.probst.docsearch.method.dao;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.framework.dao.Dao;
import codes.probst.framework.pagination.Pageable;

public interface MethodDao extends Dao<MethodModel, Long> {

	public Collection<MethodModel> findByClassId(Long classId, Pageable pageable);

	public Optional<MethodModel> findByClassIdAndNameAndReturnTypeIdAndSignature(Long classId, String name, Long returnTypeId, String signature);

}
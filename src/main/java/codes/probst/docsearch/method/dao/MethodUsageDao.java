package codes.probst.docsearch.method.dao;

import java.util.Collection;

import codes.probst.docsearch.method.model.MethodUsageModel;
import codes.probst.framework.dao.Dao;
import codes.probst.framework.pagination.Pageable;

public interface MethodUsageDao extends Dao<MethodUsageModel, Long> {

	public Collection<MethodUsageModel> findByMethodId(Long methodId, Pageable pageable);

}
package codes.probst.docsearch.method.service;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.method.dao.MethodDao;
import codes.probst.docsearch.method.dao.MethodUsageDao;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.method.model.MethodUsageModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.service.AbstractService;

public class DefaultMethodService extends AbstractService<MethodDao, MethodModel, Long> implements MethodService {
	private MethodUsageDao methodUsageDao;
	
	@Override
	public Collection<MethodModel> getByClassId(Long classId, Pageable pageable) {
		return getDao().findByClassId(classId, pageable);
	}

	@Override
	public Collection<MethodUsageModel> getUsageByMethodId(Long methodId, Pageable pageable) {
		return methodUsageDao.findByMethodId(methodId, pageable);
	}
	
	@Override
	public void save(MethodUsageModel methodUsage) {
		if(methodUsage.getId() == null) {
			methodUsageDao.insert(methodUsage);
		} else {
			methodUsageDao.update(methodUsage);
		}
	}
	
	@Override
	public Optional<MethodModel> getByClassIdAndNameAndReturnTypeIdAndSignature(Long classId, String name, Long returnTypeId, String signature) {
		return getDao().findByClassIdAndNameAndReturnTypeIdAndSignature(classId, name, returnTypeId, signature);
	}
	
	@Override
	protected boolean isNew(MethodModel object) {
		return object.getId() == null;
	}
	
	public void setMethodUsageDao(MethodUsageDao methodUsageDao) {
		this.methodUsageDao = methodUsageDao;
	}
}

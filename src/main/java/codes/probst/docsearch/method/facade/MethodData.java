package codes.probst.docsearch.method.facade;

import java.util.Collection;

public class MethodData {
	private String name;
	private String documentation;
	private String signature;
	private Collection<MethodReferenceData> references;
	private String returnTypeTag;
	private Collection<String> parameterTypeTags;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDocumentation() {
		return documentation;
	}

	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}

	public Collection<MethodReferenceData> getReferences() {
		return references;
	}

	public void setReferences(Collection<MethodReferenceData> references) {
		this.references = references;
	}
	
	public String getSignature() {
		return signature;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getReturnTypeTag() {
		return returnTypeTag;
	}
	
	public void setReturnTypeTag(String returnTypeTag) {
		this.returnTypeTag = returnTypeTag;
	}
	
	public Collection<String> getParameterTypeTags() {
		return parameterTypeTags;
	}
	
	public void setParameterTypeTags(Collection<String> parameterTypeTags) {
		this.parameterTypeTags = parameterTypeTags;
	}
}

package codes.probst.docsearch.method.facade;

import java.util.Collection;

import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.framework.facade.Facade;
import codes.probst.framework.pagination.Pageable;

public interface MethodFacade extends Facade<MethodModel, MethodData, Long> {

	public Collection<MethodData> getByClassId(Long classId, Pageable pageable, MethodPopulationOption... options);

}
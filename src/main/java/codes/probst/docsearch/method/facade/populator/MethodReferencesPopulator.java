package codes.probst.docsearch.method.facade.populator;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.method.facade.MethodData;
import codes.probst.docsearch.method.facade.MethodReferenceData;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.method.model.MethodUsageModel;
import codes.probst.docsearch.method.service.MethodService;
import codes.probst.framework.converter.Converter;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlResolver;

public class MethodReferencesPopulator implements Populator<MethodModel, MethodData> {
	private Converter<MethodUsageModel, MethodReferenceData> converter;
	private Converter<ClassModel, ClassData> classConverter;
	private UrlResolver<ClassModel> classUrlResolver;
	private MethodService methodService;
	
	@Override
	public void populate(MethodModel source, MethodData target) {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("name", SortDirection.ASCENDING) });
		
		Collection<MethodUsageModel> references = methodService.getUsageByMethodId(source.getId(), pageable);
		List<MethodReferenceData> referenceDatas = references.stream().map(method -> {
			MethodReferenceData data = converter.convert(method);
			data.setClazz(classConverter.convert(method.getCallingClass()));
			data.getClazz().setUrl(classUrlResolver.resolve(method.getCallingClass()));
			return data;
		}).collect(Collectors.toList());
		
		Collections.shuffle(referenceDatas);
		
		if(referenceDatas.size() > 5) {
			referenceDatas = referenceDatas.subList(0, 5);
		}
		
		target.setReferences(referenceDatas);
	}
	
	public void setConverter(Converter<MethodUsageModel, MethodReferenceData> converter) {
		this.converter = converter;
	}
	
	public void setClassConverter(Converter<ClassModel, ClassData> classConverter) {
		this.classConverter = classConverter;
	}
	
	public void setMethodService(MethodService methodService) {
		this.methodService = methodService;
	}
	
	public void setClassUrlResolver(UrlResolver<ClassModel> classUrlResolver) {
		this.classUrlResolver = classUrlResolver;
	}
}

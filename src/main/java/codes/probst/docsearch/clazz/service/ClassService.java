package codes.probst.docsearch.clazz.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.service.Service;

public interface ClassService extends Service<ClassModel, Long>{

	public ClassUsageModel getClassUsage(String className, ClassModel clazz);

	public void save(ClassUsageModel object);

	public Collection<ClassModel> getByPackageId(Long packageId, Pageable pageable);

	public Optional<ClassModel> getByNameAndPackageInVersions(String className, String packageName, List<Long> versionIds);
	
}
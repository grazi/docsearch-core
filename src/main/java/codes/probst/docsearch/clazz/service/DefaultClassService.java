package codes.probst.docsearch.clazz.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import codes.probst.docsearch.clazz.dao.ClassDao;
import codes.probst.docsearch.clazz.dao.ClassUsageDao;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.service.AbstractService;

public class DefaultClassService extends AbstractService<ClassDao, ClassModel, Long> implements ClassService {
	private ClassUsageDao classUsageDao;
	
	
	@Override
	public Optional<ClassModel> getByNameAndPackageInVersions(String className, String packageName, List<Long> versionIds) {
		return getDao().findByNameAndPackageInVersions(className, packageName, versionIds);
	}
	
	@Override
	public ClassUsageModel getClassUsage(String className, ClassModel clazz) {
		boolean array = false;
		if(className.endsWith("[]")) {
			array = true;
			className = className.substring(0, className.length() - 2);
		}
		
		ClassUsageModel classUsage = new ClassUsageModel();
		classUsage.setClazz(clazz);
		classUsage.setArray(array);
		return classUsage;
	}
	
	@Override
	public void save(ClassUsageModel object) {
		if(object.getId() == null) {
			classUsageDao.insert(object);
		} else {
			classUsageDao.update(object);
		}
	}
	
	@Override
	public Collection<ClassModel> getByPackageId(Long packageId, Pageable pageable) {
		return getDao().findByPackageId(packageId, pageable);
	}
	
	@Override
	protected boolean isNew(ClassModel object) {
		return object.getId() == null;
	}

	public void setClassUsageDao(ClassUsageDao classUsageDao) {
		this.classUsageDao = classUsageDao;
	}
	
}

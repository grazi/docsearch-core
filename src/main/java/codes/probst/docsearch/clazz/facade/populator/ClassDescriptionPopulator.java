package codes.probst.docsearch.clazz.facade.populator;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.framework.populator.Populator;

public class ClassDescriptionPopulator implements Populator<ClassModel, ClassData> {

	@Override
	public void populate(ClassModel source, ClassData target) {
		target.setDescription(source.getDocumentation());
	}
}

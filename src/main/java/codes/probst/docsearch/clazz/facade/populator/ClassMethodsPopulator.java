package codes.probst.docsearch.clazz.facade.populator;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.method.facade.MethodFacade;
import codes.probst.docsearch.method.facade.MethodPopulationOption;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;
import codes.probst.framework.populator.Populator;

public class ClassMethodsPopulator implements Populator<ClassModel, ClassData> {
	private BeanResolver beanResolver;
	private MethodFacade methodFacade;
	
	@Override
	public void populate(ClassModel source, ClassData target) {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("name", SortDirection.ASCENDING) });
		
		
		target.setMethods(getMethodFacade().getByClassId(source.getId(), pageable,
			MethodPopulationOption.OPTION_REFERENCES,
			MethodPopulationOption.OPTION_TAGS
		));
	}
	
	public void setBeanResolver(BeanResolver beanResolver) {
		this.beanResolver = beanResolver;
	}
	
	protected MethodFacade getMethodFacade() {
		if(methodFacade == null) {
			methodFacade = beanResolver.resolve(MethodFacade.class);
		}
		return methodFacade;
	}
}

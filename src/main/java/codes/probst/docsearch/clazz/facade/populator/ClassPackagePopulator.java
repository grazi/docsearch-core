package codes.probst.docsearch.clazz.facade.populator;

import java.util.Optional;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.facade.PackageFacade;
import codes.probst.docsearch.packages.facade.PackagePopulationOption;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.populator.Populator;

public class ClassPackagePopulator implements Populator<ClassModel, ClassData> {
	private BeanResolver beanResolver;
	private PackageFacade packageFacade;
	
	public void populate(ClassModel source, ClassData target) {
		Optional<PackageData> optional = getPackageFacade().getByClassId(source.getId(), PackagePopulationOption.OPTION_URL);
		if(optional.isPresent()) {
			target.setPackagez(optional.get());
		}
	}
	

	private PackageFacade getPackageFacade() {
		if(packageFacade == null) {
			packageFacade = beanResolver.resolve(PackageFacade.class);
		}
		return packageFacade;
	}
	
	public void setBeanResolver(BeanResolver beanResolver) {
		this.beanResolver = beanResolver;
	}
}

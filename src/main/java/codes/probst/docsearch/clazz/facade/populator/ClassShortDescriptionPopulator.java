package codes.probst.docsearch.clazz.facade.populator;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.util.DocumentationUtil;
import codes.probst.framework.populator.Populator;

public class ClassShortDescriptionPopulator implements Populator<ClassModel, ClassData> {
	private int sentences;
	
	@Override
	public void populate(ClassModel source, ClassData target) {
		String documentation = DocumentationUtil.getDocumentationPart(source.getDocumentation(), sentences);
		target.setDescription(documentation);
	}
	
	
	public void setSentences(int sentences) {
		this.sentences = sentences;
	}
}

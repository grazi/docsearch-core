package codes.probst.docsearch.clazz.facade;

import java.util.Collection;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.service.ClassService;
import codes.probst.framework.facade.AbstractFacade;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.populator.PopulationOption;

public class DefaultClassFacade extends AbstractFacade<ClassModel, ClassData, ClassService, Long> implements ClassFacade {
	
	@Override
	public Collection<ClassData> getByPackageId(Long packageId, Pageable pageable, PopulationOption... options) {
		return convertAndPopulateAll(getService().getByPackageId(packageId, pageable), options);
	}
}

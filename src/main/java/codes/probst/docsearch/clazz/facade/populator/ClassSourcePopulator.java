package codes.probst.docsearch.clazz.facade.populator;

import java.nio.charset.Charset;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.framework.populator.Populator;

public class ClassSourcePopulator implements Populator<ClassModel, ClassData> {
	
	@Override
	public void populate(ClassModel source, ClassData target) {
		if(source.getSource() != null) {
			target.setSource(new String(source.getSource(), Charset.forName("utf-8")));
		}
	}

}

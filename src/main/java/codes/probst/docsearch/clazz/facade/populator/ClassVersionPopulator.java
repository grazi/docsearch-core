package codes.probst.docsearch.clazz.facade.populator;

import java.util.Optional;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.facade.VersionFacade;
import codes.probst.docsearch.version.facade.VersionPopulationOption;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.populator.Populator;

public class ClassVersionPopulator implements Populator<ClassModel, ClassData> {
	private BeanResolver beanResolver;
	private VersionFacade versionFacade;
	
	@Override
	public void populate(ClassModel source, ClassData target) {
		Optional<VersionData> optional = getVersionFacade().getByClassId(source.getId(), VersionPopulationOption.OPTION_URL);
		if(optional.isPresent())  {
			target.setVersion(optional.get());
		}
	}

	private VersionFacade getVersionFacade() {
		if(versionFacade == null) {
			versionFacade = beanResolver.resolve(VersionFacade.class);
		}
		return versionFacade;
	}
	
	public void setBeanResolver(BeanResolver beanResolver) {
		this.beanResolver = beanResolver;
	}
}

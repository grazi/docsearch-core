package codes.probst.docsearch.clazz.facade.populator;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlResolver;

public class ClassUrlPopulator implements Populator<ClassModel, ClassData> {
	private UrlResolver<ClassModel> resolver;
	
	@Override
	public void populate(ClassModel source, ClassData target) {
		target.setUrl(resolver.resolve(source));
	}
	
	public void setResolver(UrlResolver<ClassModel> resolver) {
		this.resolver = resolver;
	}
}

package codes.probst.docsearch.clazz.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.framework.dao.Dao;
import codes.probst.framework.pagination.Pageable;

public interface ClassDao extends Dao<ClassModel, Long> {

	public Collection<ClassModel> findByPackageId(Long packageId, Pageable pageable);

	public Optional<ClassModel> findByNameAndPackageInVersions(String className, String packageName, List<Long> versionIds);
	
}

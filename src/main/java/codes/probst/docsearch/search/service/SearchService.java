package codes.probst.docsearch.search.service;

import java.util.Map;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.search.MultiSearchResult;
import codes.probst.framework.pagination.Pageable;

public interface SearchService {
	
	public MultiSearchResult getRandomClass(Pageable pageable);
	
	public MultiSearchResult search(String text, Map<String, Map<String, String>> filters, Pageable pageable);
	
	public void index(ClassModel clazz);

	public void index(PackageModel packagez);

	public void index(ProjectModel project);

}
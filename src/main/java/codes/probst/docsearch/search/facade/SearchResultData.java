package codes.probst.docsearch.search.facade;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import codes.probst.docsearch.clazz.facade.ClassData;
import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.project.facade.ProjectData;

public class SearchResultData {
	private String searchText;
	private Collection<ProjectData> projects;
	private Map<String, List<String>> projectFilters;
	private long totalProjects;
	private Collection<PackageData> packages;
	private Map<String, List<String>> packageFilters;
	private long totalPackages;
	private Collection<ClassData> classes;
	private Map<String, List<String>> classFilters;
	private long totalClasses;

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public Collection<ProjectData> getProjects() {
		return projects;
	}

	public void setProjects(Collection<ProjectData> projects) {
		this.projects = projects;
	}

	public long getTotalProjects() {
		return totalProjects;
	}

	public void setTotalProjects(long totalProjects) {
		this.totalProjects = totalProjects;
	}

	public Collection<PackageData> getPackages() {
		return packages;
	}

	public void setPackages(Collection<PackageData> packages) {
		this.packages = packages;
	}

	public long getTotalPackages() {
		return totalPackages;
	}

	public void setTotalPackages(long totalPackages) {
		this.totalPackages = totalPackages;
	}

	public Collection<ClassData> getClasses() {
		return classes;
	}

	public void setClasses(Collection<ClassData> classes) {
		this.classes = classes;
	}

	public long getTotalClasses() {
		return totalClasses;
	}

	public void setTotalClasses(long totalClasses) {
		this.totalClasses = totalClasses;
	}

	public long getTotal() {
		return totalClasses + totalPackages + totalProjects;
	}

	public Map<String, List<String>> getProjectFilters() {
		return projectFilters;
	}

	public void setProjectFilters(Map<String, List<String>> projectFilters) {
		this.projectFilters = projectFilters;
	}

	public Map<String, List<String>> getPackageFilters() {
		return packageFilters;
	}

	public void setPackageFilters(Map<String, List<String>> packageFilters) {
		this.packageFilters = packageFilters;
	}

	public Map<String, List<String>> getClassFilters() {
		return classFilters;
	}

	public void setClassFilters(Map<String, List<String>> classFilters) {
		this.classFilters = classFilters;
	}

}

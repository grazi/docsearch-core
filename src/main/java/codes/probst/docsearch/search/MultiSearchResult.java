package codes.probst.docsearch.search;

import java.util.Map;

public class MultiSearchResult {
	private Map<String, SearchResult> results;
	
	public MultiSearchResult(Map<String, SearchResult> result) {
		this.results = result;
	}
	
	public long getTotal() {
		return results.values().stream().mapToLong(SearchResult::getTotal).sum();
	}
	
	public SearchResult	 getResult(String name) {
		return results.get(name);
	}
}

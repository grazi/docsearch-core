package codes.probst.docsearch.search.index;

public interface IndexStrategy {

	public void index(IndexType type);

}
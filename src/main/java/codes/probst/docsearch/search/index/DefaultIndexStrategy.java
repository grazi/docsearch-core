package codes.probst.docsearch.search.index;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.service.ClassService;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.service.PackageService;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.search.service.SearchService;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.docsearch.version.service.VersionService;

public class DefaultIndexStrategy implements IndexStrategy {
	private static final Logger LOG = LoggerFactory.getLogger(DefaultIndexStrategy.class);
	
	private SearchService searchService;
	private VersionService versionService;
	private PackageService packageService;
	private ClassService classService;
	private ThreadLocal<AtomicInteger> counterThreadLocale= new ThreadLocal<AtomicInteger>() {
		
		protected AtomicInteger initialValue() {
			return new AtomicInteger(0);
		}
		
	};
	
	@Override
	public void index(IndexType type) {
		Collection<VersionModel> versions = null;
		switch(type) {
			case FULL:		versions = versionService.getAll(null);
							break;
			case UPDATE:	versions = versionService.getAllUnexported(null);
							break;
			default:		versions = Collections.emptyList();
							break;
		}
		
		
		Map<Long, ProjectModel> projects = new HashMap<>();
		for(VersionModel version : versions) {
			projects.put(version.getProject().getId(), version.getProject());
		}
		
		try {
			projects.values().forEach(this::handleProject);
			versions.forEach(this::handleVersion);
			
		
			LOG.info("indexed {} documents.", counterThreadLocale.get().get());
		} finally {
			counterThreadLocale.remove();
		}
	}
	
	private void handleProject(ProjectModel project) {
		counterThreadLocale.get().incrementAndGet();
		searchService.index(project);
	}
	
	private void handleVersion(VersionModel version) {
		Collection<PackageModel> packages = packageService.getByVersionId(version.getId(), null);
		packages.forEach(this::handlePackage);
		
		version.setExported(true);
		versionService.save(version);
	}
	
	private void handlePackage(PackageModel packagez) {
		counterThreadLocale.get().incrementAndGet();
		searchService.index(packagez);
		Collection<ClassModel> classes = classService.getByPackageId(packagez.getId(), null);
		classes.forEach(this::handleClass);
	}
	
	private void handleClass(ClassModel clazz) {
		counterThreadLocale.get().incrementAndGet();
		searchService.index(clazz);
	}
	
	public void setClassService(ClassService classService) {
		this.classService = classService;
	}
	
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
	public void setPackageService(PackageService packageService) {
		this.packageService = packageService;
	}
	
	public void setVersionService(VersionService versionService) {
		this.versionService = versionService;
	}
}

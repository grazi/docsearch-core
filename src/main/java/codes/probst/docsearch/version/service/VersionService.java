package codes.probst.docsearch.version.service;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.service.Service;

public interface VersionService extends Service<VersionModel, Long>{

	public abstract Collection<VersionModel> getByProjectId(Long projectId);

	public abstract Optional<VersionModel> getByPackageId(Long packageId);

	public abstract Optional<VersionModel> getByClassId(Long classId);
	
	public abstract Optional<VersionModel> getByFullQualifier(String groupId, String artifactId, String versionCode);

	public abstract Collection<VersionModel> getAllUnexported(Pageable pageable);


}
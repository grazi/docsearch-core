package codes.probst.docsearch.version.dao;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.dao.Dao;
import codes.probst.framework.pagination.Pageable;

public interface VersionDao extends Dao<VersionModel, Long> {

	public Collection<VersionModel> findByProjectId(Long projectId);

	public Optional<VersionModel> findByFullQualifier(String groupId, String artifactId, String versionCode);

	public Optional<VersionModel> findByClassId(Long classId);

	public Optional<VersionModel> findByPackageId(Long packageId);

	public Collection<VersionModel> findAllUnexported(Pageable pageable);
	
}

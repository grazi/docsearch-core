package codes.probst.docsearch.version.facade;

import codes.probst.framework.populator.PopulationOption;

public class VersionPopulationOption extends PopulationOption {
	public static final VersionPopulationOption OPTION_URL = new VersionPopulationOption("URL", 1);
	public static final VersionPopulationOption OPTION_PACKAGES = new VersionPopulationOption("PACKAGES", 1);
	public static final VersionPopulationOption OPTION_PROJECT = new VersionPopulationOption("PROJECT", 1);
	
	protected VersionPopulationOption(String code, int order) {
		super("VERSION_POPULATION_OPTION_" + code, order);
	}

	
}

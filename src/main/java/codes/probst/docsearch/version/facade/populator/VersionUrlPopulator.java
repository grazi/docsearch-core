package codes.probst.docsearch.version.facade.populator;

import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlResolver;

public class VersionUrlPopulator implements Populator<VersionModel, VersionData> {
	private UrlResolver<VersionModel> resolver;
	
	@Override
	public void populate(VersionModel source, VersionData target) {
		target.setUrl(resolver.resolve(source));
	}
	
	public void setResolver(UrlResolver<VersionModel> resolver) {
		this.resolver = resolver;
	}

}

package codes.probst.docsearch.version.facade.converter;

import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.converter.Converter;

public class DefaultVersionConverter implements Converter<VersionModel, VersionData> {

	public VersionData convert(VersionModel value) {
		VersionData data = new VersionData();
		
		data.setId(value.getId());
		data.setCode(value.getCode());
		
		return data;
	}
	
}

package codes.probst.docsearch.version.facade.populator;

import codes.probst.docsearch.packages.facade.PackageFacade;
import codes.probst.docsearch.packages.facade.PackagePopulationOption;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;
import codes.probst.framework.populator.Populator;

public class VersionPackagesPopulator implements Populator<VersionModel, VersionData>{
	private BeanResolver beanResolver;
	private PackageFacade packageFacade;
	
	@Override
	public void populate(VersionModel source, VersionData target) {
		Pageable pageable = new Pageable();
		pageable.setSort(new Sort[] { new Sort("name", SortDirection.ASCENDING) });
		
		target.setPackages(getPackageFacade().getByVersionId(source.getId(), pageable,
			PackagePopulationOption.OPTION_URL,
			PackagePopulationOption.OPTION_SHORT_DOCUMENTATION)
		);
	}
	
	public void setBeanResolver(BeanResolver beanResolver) {
		this.beanResolver = beanResolver;
	}
	
	private PackageFacade getPackageFacade() {
		if(packageFacade == null) {
			packageFacade = beanResolver.resolve(PackageFacade.class);
		}
		return packageFacade;
	}
}

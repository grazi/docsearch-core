package codes.probst.docsearch.doclet;

import java.util.List;

public class Package {
	private String name;
	private String documentation;
	private List<TagInfo> tags;

	public Package(String name, String documentation, List<TagInfo> tags) {
		this.name = name;
		this.documentation = documentation;
		this.tags = tags;
	}

	public String getDocumentation() {
		return documentation;
	}
	
	public String getName() {
		return name;
	}
	
	public List<TagInfo> getTags() {
		return tags;
	}
}

package codes.probst.docsearch.doclet;

public class TagInfo {
	private String kind;
	private String text;
	
	public TagInfo(String kind, String text) {
		this.kind = kind;
		this.text = text;
	}

	public String getKind() {
		return kind;
	}

	public String getText() {
		return text;
	}

}

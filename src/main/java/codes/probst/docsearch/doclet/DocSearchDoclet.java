package codes.probst.docsearch.doclet;

import com.sun.javadoc.RootDoc;

public class DocSearchDoclet {

	
	/**
	 * Processes the JavaDoc documentation.
	 * <p>
	 * This method is required for all doclets.
	 * 
	 * @see com.sun.javadoc.Doclet#start(RootDoc)
	 * 
	 * @param rootDoc
	 *            The root of the documentation tree.
	 * 
	 * @return <code>true</code> if processing was successful.
	 */
	public static boolean start(RootDoc rootDoc) {
		synchronized (DocSearchDoclet.class) {
			Parser.getInstance().parseRootDoc(rootDoc);
		}
		
		return true;
	}
}

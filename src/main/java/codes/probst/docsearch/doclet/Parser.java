package codes.probst.docsearch.doclet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.PackageDoc;
import com.sun.javadoc.RootDoc;
import com.sun.javadoc.Tag;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassUsageModel;
import codes.probst.docsearch.clazz.service.ClassService;
import codes.probst.docsearch.method.model.MethodModel;
import codes.probst.docsearch.method.service.MethodService;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.service.PackageService;

public class Parser {
	private static final Parser INSTANCE = new Parser();
	
	private Long versionId;
	private Map<String, ClassModel> classesCache;
	private Map<String, PackageModel> packagesCache;
	private Map<Long, Collection<MethodModel>> methodesCache;
	
	private PackageService packageService;
	private ClassService classService;
	private MethodService methodService;
	
	private Parser() {
	}
	
	public static Parser getInstance() {
		return INSTANCE;
	}
	
	public static void release() {
		INSTANCE.versionId = null;
		INSTANCE.classesCache = null;
		INSTANCE.packagesCache = null;
	}
	
	public static void init(Long versionId, Map<String, PackageModel> packagesCache, Map<String, ClassModel> classesCache, Map<Long, Collection<MethodModel>> methodesCache) {
		if(INSTANCE.versionId != null) {
			throw new IllegalAccessError("versionId already set. call relase before use it");
		}
		INSTANCE.versionId = versionId;
		INSTANCE.packagesCache = packagesCache;
		INSTANCE.classesCache = classesCache;
		INSTANCE.methodesCache = methodesCache;
	}
	
	public static void setup(PackageService packageService, ClassService classService, MethodService methodService) {
		INSTANCE.packageService = packageService;
		INSTANCE.classService = classService;
		INSTANCE.methodService = methodService;
	}
	
	public synchronized void parseRootDoc(RootDoc rootDoc) {
		Set<String> parsedPackages = new HashSet<>();
		
		ClassDoc[] classDocs =  rootDoc.classes();
		for (int i = 0; i < classDocs.length; i++) {
			ClassDoc classDoc = classDocs[i];
			PackageDoc packageDoc = classDoc.containingPackage();
			
			if(!parsedPackages.contains(packageDoc.name())) {
				handlePackage(packageDoc);
			}
			
			handleClass(classDoc);
		}
	}
	
	private void handlePackage(PackageDoc packageDoc) {
		PackageModel packagez = packagesCache.get(packageDoc.name());
		if(packagez != null) {
			packagez.setDocumentation(packageDoc.commentText());
			packageService.save(packagez);
		}
	}
	
	private void handleClass(ClassDoc classDoc) {
		ClassModel clazz = classesCache.get(classDoc.qualifiedName());
		if(clazz != null) {
			clazz.setDocumentation(classDoc.commentText());

			Collection<MethodModel> methods = methodesCache.get(clazz.getId());
			for(MethodDoc methodDoc : classDoc.methods()) {
				handleMethod(methodDoc, methods);
			}
			
			classService.save(clazz);
		}
	}
	
	private void handleMethod(MethodDoc methodDoc, Collection<MethodModel> methods) {
		Optional<MethodModel> optionalMethod = methods.stream()
			.filter(method -> method.getName().equals(methodDoc.name()))
			.filter(method -> method.getSignature().substring(method.getName().length()).equals(methodDoc.signature()))
			.findAny();
		
		if(optionalMethod.isPresent()) {
			MethodModel method = optionalMethod.get();
			method.setDocumentation(methodDoc.commentText());
			
			if(method.getParameters() != null) {
				List<TagInfo> tags = parseTags(methodDoc.tags());
				TagInfo returnTagInfo = null;
				List<TagInfo> parameterTagInfo = new ArrayList<>(Math.max(0, tags.size() - 1));
				
				for(TagInfo tag : tags) {
					switch(tag.getKind()) {
						case "@param": 	parameterTagInfo.add(tag);
										break;
						case "@return": returnTagInfo = tag;
										break;
					}
				}
				
				Iterator<ClassUsageModel> it = method.getParameters().iterator();
				for(int i = 0; it.hasNext() && i < parameterTagInfo.size(); i++) {
					ClassUsageModel classUsage = it.next();
					classUsage.setDocumentation(parameterTagInfo.get(i).getText());
					classService.save(classUsage);
				}
				
				if(returnTagInfo != null) {
					ClassUsageModel returnType = method.getReturnType();
					returnType.setDocumentation(returnTagInfo.getText());
					classService.save(returnType);
				}
				
			}
			methodService.save(method);;
		}
	}
	
	private List<TagInfo> parseTags(Tag[] tags) {
		return Arrays.stream(tags).map(tag -> new TagInfo(tag.kind(), tag.text())).collect(Collectors.toList());
	}
}

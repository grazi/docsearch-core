package codes.probst.docsearch.packages.url;

import java.util.HashMap;
import java.util.Map;

import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.docsearch.version.model.VersionModel;
import codes.probst.framework.url.AbstractUrlResolver;

public class DefaultPackageUrlResolver extends AbstractUrlResolver<PackageModel> {

	@Override
	protected Map<String, String> getPathParameters(PackageModel value) {
		Map<String, String> parameters = new HashMap<>();
		
		parameters.put("package-id", value.getId().toString());
		parameters.put("package-name", value.getName());
		
		VersionModel version = value.getVersion();
		if(version != null) {
			parameters.put("version-id", version.getId().toString());
			parameters.put("version-code", version.getCode());
			
			ProjectModel project = version.getProject();
			if(project != null) {
				parameters.put("project-id", project.getId().toString());
				parameters.put("project-group-id", project.getGroupId());
				parameters.put("project-artifact-id", project.getArtifactId());
			}
		}
		
		return parameters;
	}
	
	protected String getPattern() {
		return "/projects/{project-artifact-id}/{project-id}/{version-code}/{version-id}/{package-name}/{package-id}";
	}
}

package codes.probst.docsearch.packages.dao;

import java.util.Collection;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.model.ClassModel_;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.model.PackageModel_;
import codes.probst.framework.dao.AbstractJpaDao;
import codes.probst.framework.pagination.Pageable;

public class JpaPackageDao extends AbstractJpaDao<PackageModel, Long> implements PackageDao {

	public JpaPackageDao() {
		super(PackageModel.class, PackageModel_.id);
	}
	
	@Override
	public Collection<PackageModel> findByVersionId(Long versionId, Pageable pageable) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<PackageModel> criteria = builder.createQuery(PackageModel.class);
		criteria.where(builder.equal(criteria.from(PackageModel.class).get(PackageModel_.version), versionId));
		return select(criteria, pageable);
	}

	@Override
	public Optional<PackageModel> findByNameAndVersionId(String name, Long versionId) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<PackageModel> criteria = builder.createQuery(PackageModel.class);
		Root<PackageModel> root = criteria.from(PackageModel.class);
		criteria.select(root);
		criteria.where(
			builder.and(
				builder.equal(root.get(PackageModel_.name), name),
				builder.equal(root.get(PackageModel_.version), versionId)
			)
		);
		return selectOnce(criteria);
	}
	
	@Override
	public Optional<PackageModel> findByClassId(Long classId) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<PackageModel> criteria = builder.createQuery(PackageModel.class);
		Root<PackageModel> root = criteria.from(PackageModel.class);
		Join<PackageModel, ClassModel> join = root.join(PackageModel_.classes);
		criteria.select(root);
		criteria.where(
			builder.equal(join.get(ClassModel_.id), classId)
		);
		return selectOnce(criteria);
	}
}

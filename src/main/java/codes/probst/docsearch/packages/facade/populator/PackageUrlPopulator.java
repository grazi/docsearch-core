package codes.probst.docsearch.packages.facade.populator;

import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.url.UrlResolver;

public class PackageUrlPopulator implements Populator<PackageModel, PackageData> {
	private UrlResolver<PackageModel> resolver;
	
	@Override
	public void populate(PackageModel source, PackageData target) {
		target.setUrl(resolver.resolve(source));
	}
	
	public void setResolver(UrlResolver<PackageModel> resolver) {
		this.resolver = resolver;
	}
}

package codes.probst.docsearch.packages.facade.populator;

import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.framework.populator.Populator;

public class PackageDocumentationPopulator implements Populator<PackageModel, PackageData> {

	@Override
	public void populate(PackageModel source, PackageData target) {
		target.setDocumentation(source.getDocumentation() == null ? "" : source.getDocumentation());
	}
}

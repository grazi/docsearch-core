package codes.probst.docsearch.packages.facade.populator;

import java.util.Optional;

import codes.probst.docsearch.packages.facade.PackageData;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.version.facade.VersionData;
import codes.probst.docsearch.version.facade.VersionFacade;
import codes.probst.docsearch.version.facade.VersionPopulationOption;
import codes.probst.framework.bean.BeanResolver;
import codes.probst.framework.populator.Populator;

public class PackageVersionPopulator implements Populator<PackageModel, PackageData> {
	private BeanResolver beanResolver;
	private VersionFacade versionFacade;

	@Override
	public void populate(PackageModel source, PackageData target) {
		Optional<VersionData> optional = getVersionFacade().getByPackageId(source.getId(), VersionPopulationOption.OPTION_URL, VersionPopulationOption.OPTION_PROJECT);
		
		if(optional.isPresent()) {
			target.setVersion(optional.get());
		}
	}
	
	private VersionFacade getVersionFacade() {
		if(versionFacade == null) {
			versionFacade = beanResolver.resolve(VersionFacade.class);
		}
		return versionFacade;
	}
	
	
	public void setBeanResolver(BeanResolver beanResolver) {
		this.beanResolver = beanResolver;
	}
	
	
}

package codes.probst.docsearch.packages.facade;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.service.PackageService;
import codes.probst.framework.facade.AbstractFacade;
import codes.probst.framework.pagination.Pageable;

public class DefaultPackageFacade extends AbstractFacade<PackageModel, PackageData, PackageService, Long> implements PackageFacade {

	@Override
	public Collection<PackageData> getByVersionId(Long versionId, Pageable pageable, PackagePopulationOption... options) {
		return convertAndPopulateAll(getService().getByVersionId(versionId, pageable), options);
	}
	
	@Override
	public Optional<PackageData> getByClassId(Long classId, PackagePopulationOption... options) {
		Optional<PackageModel> optional = getService().getByClassId(classId);
		
		if(optional.isPresent()) {
			return Optional.of(convertAndPopulate(optional.get(), options));
		}
		return Optional.empty();
	}
}

package codes.probst.docsearch.packages.service;

import java.util.Collection;
import java.util.Optional;

import codes.probst.docsearch.packages.dao.PackageDao;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.service.AbstractService;

public class DefaultPackageService extends AbstractService<PackageDao, PackageModel, Long> implements PackageService {

	@Override
	protected boolean isNew(PackageModel object) {
		return object.getId() == null;
	}
	
	@Override
	public Collection<PackageModel> getByVersionId(Long versionId, Pageable pageable) {
		return getDao().findByVersionId(versionId, pageable);
	}
	
	@Override
	public Optional<PackageModel> getByClassId(Long classId) {
		return getDao().findByClassId(classId);
	}

	@Override
	public Optional<PackageModel> getByNameAndVersionId(String name, Long versionId) {
		return getDao().findByNameAndVersionId(name, versionId);
	}
}

package codes.probst.docsearch.queue.dao;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import codes.probst.docsearch.queue.model.QueueEntryModel;
import codes.probst.docsearch.queue.model.QueueEntryModel_;
import codes.probst.docsearch.queue.model.QueueEntryState;
import codes.probst.framework.dao.AbstractJpaDao;
import codes.probst.framework.pagination.Pageable;

/**
 * {@code QueueEntryDao} implementation based on the Java Persistance Framework.
 * @author Benjamin Probst
 *
 */
public class JpaQueueEntryDao extends AbstractJpaDao<QueueEntryModel, Long> implements QueueEntryDao {

	public JpaQueueEntryDao() {
		super(QueueEntryModel.class, QueueEntryModel_.id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<QueueEntryModel> findFirstWithState(QueueEntryState[] states, Pageable pageable) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<QueueEntryModel> criteria = builder.createQuery(QueueEntryModel.class);
		Root<QueueEntryModel> root = criteria.from(QueueEntryModel.class);
		criteria.where(root.get(QueueEntryModel_.state).in((Object[]) states));
		
		pageable.setCurrentPage(0);
		pageable.setPageSize(1);
		
		List<QueueEntryModel> result = select(criteria, pageable);
		return result == null || result.isEmpty() ? Optional.empty() : Optional.of(result.get(0));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<QueueEntryModel> findFirstByDependencyAndState(String groupId, String artifactId, String version, QueueEntryState... states) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<QueueEntryModel> criteria = builder.createQuery(QueueEntryModel.class);
		Root<QueueEntryModel> root = criteria.from(QueueEntryModel.class);
		criteria.where(builder.and(
			root.get(QueueEntryModel_.state).in((Object[]) states),
			builder.equal(root.get(QueueEntryModel_.groupId), groupId),
			builder.equal(root.get(QueueEntryModel_.artifactId), artifactId),
			builder.equal(root.get(QueueEntryModel_.version), version)
		));
		
		Pageable pageable = new Pageable();
		pageable.setCurrentPage(0);
		pageable.setPageSize(1);
		
		List<QueueEntryModel> result = select(criteria, pageable);
		return result == null || result.isEmpty() ? Optional.empty() : Optional.of(result.get(0));
	}
	
	@Override
	public void deleteBefore(Instant instant, QueueEntryState... states) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaDelete<QueueEntryModel> criteria = builder.createCriteriaDelete(QueueEntryModel.class);
		Root<QueueEntryModel> root = criteria.from(QueueEntryModel.class);
		criteria.where(
			builder.and(
				root.get(QueueEntryModel_.state).in((Object[]) states),
				builder.lessThan(root.get(QueueEntryModel_.modificationDate), instant)
			)
		);
		delete(criteria);
	}

}

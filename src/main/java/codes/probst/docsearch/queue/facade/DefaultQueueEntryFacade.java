package codes.probst.docsearch.queue.facade;

import codes.probst.docsearch.queue.service.QueueEntryService;

public class DefaultQueueEntryFacade implements QueueEntryFacade {
	private QueueEntryService queueEntryService;
	
	@Override
	public void importEntry(byte[] compiled, byte[] source) {
		queueEntryService.importEntry(compiled, source);
	}
	
	public void setQueueEntryService(QueueEntryService queueEntryService) {
		this.queueEntryService = queueEntryService;
	}
	
}

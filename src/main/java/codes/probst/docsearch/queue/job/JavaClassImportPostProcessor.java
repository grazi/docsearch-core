package codes.probst.docsearch.queue.job;

import java.util.Arrays;
import java.util.Map;

import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.clazz.service.ClassService;
import codes.probst.docsearch.packages.model.PackageModel;
import codes.probst.docsearch.packages.service.PackageService;
import codes.probst.docsearch.version.model.VersionModel;

public class JavaClassImportPostProcessor implements ClassImportPostProcessor{
	private ClassService classService;
	private PackageService packageService;
	
	
	@Override
	public void process(ProjectType type, VersionModel version, Map<String, ClassModel> classesCache) {
		if(ProjectType.JAVA.equals(type)) {
				PackageModel packagez = new PackageModel();
			packagez.setName("");
			packagez.setVersion(version);
			packageService.save(packagez);
			
			for(Class<?> primitive : Arrays.asList(
				Boolean.TYPE,
				Character.TYPE,
				Byte.TYPE,
				Short.TYPE,
				Integer.TYPE,
				Long.TYPE,
				Float.TYPE,
				Double.TYPE,
				Void.TYPE
			)) {
				ClassModel clazz = new ClassModel();
				clazz.setName(primitive.getName());
				clazz.setPackageModel(packagez);
				classService.save(clazz);
				classesCache.put(clazz.getName(), clazz);
			}
		}
	}
	
	public void setClassService(ClassService classService) {
		this.classService = classService;
	}
	
	public void setPackageService(PackageService packageService) {
		this.packageService = packageService;
	}
}

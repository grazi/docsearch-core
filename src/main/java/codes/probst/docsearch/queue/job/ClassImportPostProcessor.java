package codes.probst.docsearch.queue.job;

import java.util.Map;

import codes.probst.docsearch.analyzer.projectinformation.ProjectType;
import codes.probst.docsearch.clazz.model.ClassModel;
import codes.probst.docsearch.version.model.VersionModel;

public interface ClassImportPostProcessor {

	public void process(ProjectType type, VersionModel version, Map<String, ClassModel> classesCache);
	
}

package codes.probst.docsearch.queue.service;

import codes.probst.docsearch.queue.model.QueueEntryModel;

public interface QueueEntryProcessor {

	void process(QueueEntryModel queueEntry);

}
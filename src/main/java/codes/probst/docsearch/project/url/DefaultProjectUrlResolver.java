package codes.probst.docsearch.project.url;

import java.util.HashMap;
import java.util.Map;

import codes.probst.docsearch.project.model.ProjectModel;

import codes.probst.framework.url.AbstractUrlResolver;

public class DefaultProjectUrlResolver extends AbstractUrlResolver<ProjectModel> {

	@Override
	protected Map<String, String> getPathParameters(ProjectModel value) {
		Map<String, String> parameters = new HashMap<>();
		
		parameters.put("project-id", value.getId().toString());
		parameters.put("project-group", value.getGroupId());
		parameters.put("project-name", value.getArtifactId());
		
		return parameters;
	}
	
	protected String getPattern() {
		return "/projects/{project-name}/{project-id}";
	}
}

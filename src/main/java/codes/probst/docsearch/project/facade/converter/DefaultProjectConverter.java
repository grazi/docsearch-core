package codes.probst.docsearch.project.facade.converter;

import codes.probst.docsearch.project.facade.ProjectData;
import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.framework.converter.Converter;

public class DefaultProjectConverter implements Converter<ProjectModel, ProjectData> {

	@Override
	public ProjectData convert(ProjectModel value) {
		ProjectData data = new ProjectData();
		
		data.setId(value.getId());
		data.setGroupId(value.getGroupId());
		data.setArtifactId(value.getArtifactId());
		
		return data;
	}
}

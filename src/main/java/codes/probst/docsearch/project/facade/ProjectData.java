package codes.probst.docsearch.project.facade;

import java.util.Collection;

import codes.probst.docsearch.version.facade.VersionData;

public class ProjectData {
	private Long id;
	private String groupId;
	private String artifactId;
	private String url;
	private Collection<VersionData> versions;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getGroupId() {
		return groupId;
	}
	
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	public String getArtifactId() {
		return artifactId;
	}
	
	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setVersions(Collection<VersionData> versions) {
		this.versions = versions;
	}
	
	public Collection<VersionData> getVersions() {
		return versions;
	}
}

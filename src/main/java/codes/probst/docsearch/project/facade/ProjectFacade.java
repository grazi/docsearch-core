package codes.probst.docsearch.project.facade;

import java.util.Optional;

import codes.probst.docsearch.project.model.ProjectModel;
import codes.probst.framework.facade.Facade;

public interface ProjectFacade extends Facade<ProjectModel, ProjectData, Long> {

	public Optional<ProjectData> getByClassId(Long classId, ProjectPopulationOption... options);

	public Optional<ProjectData> getByVersionId(Long versionId, ProjectPopulationOption... options);
	
}